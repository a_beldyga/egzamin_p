# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from form import *
from models import *
import tasks
from django.http import HttpResponse, HttpResponseRedirect
import memcache
from celery.result import AsyncResult
import time
from django.utils import timezone
import requests
import datetime


# Create your views here.

mc = memcache.Client(['194.29.175.241:11211'])


def main(request):


    return render(request, 'base.html', {'success': 'fail'})



def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return redirect(reverse('main'))

def authentication(request):
    if request.method == 'GET':
        c = {}
        c.update(csrf(request))
        return render_to_response('login.html', c)
    if request.method == 'POST':
        form=AuthForm(request.POST)
        if form.is_valid():
            # the password verified for the user
            user = form.login(request)
            if user.is_active:
                print("User is valid, active and authenticated")
                login(request, user)
                return redirect(reverse('main'))
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            # the authentication system was unable to verify the username and password
            print("The username and password were incorrect.")
            return render_to_response('login.html', {'form': form}, context_instance=RequestContext(request))


def canvas(request):


    if request.method == 'GET':
            c = {}
            c.update(csrf(request))
            return render_to_response('canvas.html', c)
    if request.method == 'POST':
            form = CanvasForm(request.POST) # A form bound to the POST data
            user=request.user
            login = form.data['login']
            password = form.data['password']


            if(UserProfile.objects.filter(user=request.user.id).exists()):
                instance = UserProfile.objects.get(user=request.user.id)
                instance.delete()


            r = UserProfile(user=user, login = login, password = password)
            r.save()



            return redirect(reverse('main'))


    else:
        return redirect(reverse('blog'))


def lista(request):

    if zaloguj(request.session.get('clogin'), request.session.get('cpassword')):
        log = request.session.get('clogin')
        pas = request.session.get('cpassword')
        r = tasks.get_courses.delay(log, pas)
        return HttpResponseRedirect(reverse('lista_progress') + '?proc=' + r.id)
    else:
        return render(request, 'lista.html', {'blad': 'Blad logowania'})


def lista_progress(request):


    if 'proc' in request.GET:
            job_id = request.GET['proc']
    else:
            return HttpResponse('Nie kombinuj')

    job = AsyncResult(job_id)
    time.sleep(1)

    if job.state == 'PENDING':
            return render(request, 'lista.html', {'result': -1, 'status': 'Oczekiwanie na workera'})

    elif job.state == 'PROGRESS':
            try:
                result = job.result['current']
            except:
                result = 99
            return render(request, 'lista.html', {'result': result, 'status': 'Pobieranie kursow...'})

    elif job.state == 'SUCCESS':

        return render(request, 'lista.html', {'kursy': job.result['kursy'], 'slownik': job.result['slownik'], 'result': 100})

    else:
        return render(request, 'lista.html', {'result': 'blad'})


def zip(request, kurs):
    # user = UserProfile.objects.get(user = request.user.id)
    # log = user.login
    # pas = user.password

    if zaloguj(request.session.get('clogin'), request.session.get('cpassword')):
        log = request.session.get('clogin')
        pas = request.session.get('cpassword')

        aktywnosc = mc.get('aktywnosc_%s' % str(kurs))
        prace_domowe = mc.get('prace_domowe_%s' % str(kurs))
        egzamin = mc.get('egzamin_%s' % str(kurs))
        laboratoria = mc.get('laboratoria_%s' % str(kurs))

        if not aktywnosc:
            r = tasks.get_grades.delay(log, pas, kurs)
            while r.result is None:
                pass



            aktywnosc = r.result['aktywnosc']
            mc.set('aktywnosc_%s' % str(kurs), aktywnosc, 300)
            prace_domowe = r.result['prace_domowe']
            mc.set('prace_domowe_%s' % str(kurs), prace_domowe, 300)
            egzamin = r.result['egzamin']
            mc.set('egzamin_%s' % str(kurs), egzamin, 300)
            laboratoria = r.result['laboratoria']
            mc.set('laboratoria_%s' % str(kurs), laboratoria, 300)


        l= tasks.create_zip.delay(aktywnosc, prace_domowe, egzamin, laboratoria)

        while l.result is None:
            pass


        response = HttpResponse(l.result, content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=myfile.zip'
        return response
    else:
       return render(request, 'list.html', {'blad': 'Blad logowania'})


def czas(request):
    if request.POST:
        if zaloguj(request.POST.get('login'), request.POST.get('password')):
            request.session['clogin'] = request.POST.get('login')
            request.session['cpassword'] = request.POST.get('password')
            request.session['poczatek'] = timezone.now()
            request.session.set_expiry(60*2)
            return render(request, 'base.html', {'success': 'Zalogowano', 'data': request.session['poczatek']})
        else:
            return render(request, 'base.html', {'success': 'Bledne dane logowania'})
    return render(request, 'czas.html', {'success': ''})


def zaloguj(login, password):
    url = {
        'login': 'https://canvas.instructure.com/login',
        'grades': 'https://canvas.instructure.com/grades'
    }
    payload = {
        'pseudonym_session[unique_id]': login,
        'pseudonym_session[password]': password
    }
    session = requests.session()
    session.post(url["login"], data=payload)
    r = session.get(url["grades"])

    if r.url == 'https://canvas.instructure.com/login':
        return False
    else:
        return True


def sesja(request):
    if request.POST:
        request.session.set_expiry(60*2)
        request.session['poczatek'] = timezone.now()
    if request.session.get('poczatek')>timezone.now() - datetime.timedelta(minutes=2):
        return render(request, 'sesja.html', {'czas': request.session.get_expiry_date()})
    else:

        return redirect(reverse('czas'))