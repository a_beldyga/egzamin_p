# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import zipfile
import StringIO
from celery import shared_task, current_task
import time
from django.utils import timezone
import datetime
from datetime import date

@shared_task()
def get_courses(login, password):

    current_task.update_state(state='PROGRESS', meta={'current': 0, 'total': 100})
    time.sleep(0.5)


    url = {
        'login': 'https://canvas.instructure.com/login',
        'courses': 'https://canvas.instructure.com/courses'
    }

    payload = {
        'pseudonym_session[unique_id]': login,
        'pseudonym_session[password]': password
    }

    # Logowanie
    session = requests.session()
    session.post(url["login"], data=payload)

    current_task.update_state(state='PROGRESS', meta={'current': 25, 'total': 100})
    time.sleep(0.5)

    # Pobranie strony z kursami

    r = session.get(url["courses"])

    # Odczyt strony

    html = r.text

    # Parsowanie strony

    bs = BeautifulSoup(html, "html5lib")

    # Scraping

    courses = []
    href =[]
  #   div2 = div.find('ul', 'unstyled_list context_list current_enrollments')

    current_task.update_state(state='PROGRESS', meta={'current': 50, 'total': 100})
    time.sleep(0.5)


    div = bs.find("div", id="content")
    adresy = div.find_all('a', href=True)
    kursy = div.find_all('span', 'name ellipsis')


    for i in range(0, len(adresy)-2):
        href.append(adresy[i]['href'])


    for i in range(0, len(kursy)-2):
        courses.append(kursy[i].text)

    slownik = {}

    current_task.update_state(state='PROGRESS', meta={'current': 75, 'total': 100})
    time.sleep(0.5)

    for i in range (0, len(courses)):
        kurs = courses[i]
        adres = href[i]
        slownik[kurs] = adres


    dl = len(courses)

    return {'kursy': courses, 'slownik': slownik, 'asresy': href, 'dlugosc': dl}

@shared_task()
def get_grades(login, password, link):

    u = "https://canvas.instructure.com/courses/" + link + "/grades"

    url = {
    'login': 'https://canvas.instructure.com/login',
    'grades': u
    }

    payload = {
        'pseudonym_session[unique_id]': login,
        'pseudonym_session[password]': password
    }


    # Logowanie
    session = requests.session()
    session.post(url["login"], data=payload)

    #current_task.update_state(state='PROGRESS', meta={'current': 25, 'total': 100})
    #time.sleep(0.5)

    # Pobranie strony z ocenami

    r = session.get(url["grades"])


    # Odczyt strony

    html = r.text


    # Parsowanie strony

    bs = BeautifulSoup(html, "html5lib")


    # Scraping

    akty = []
    lab = []
    pd = []
    egz = []


    #current_task.update_state(state='PROGRESS', meta={'current': 50, 'total': 100})
    #time.sleep(0.5)

    table = bs.find("table", id="grades_summary")
    kategorie = table.find_all("div", "context")
    th = table.find_all("th", "title")
    scores = table.find_all("span", "score")
    termin = table.find_all("td", "due")


    for i in range(0, len(kategorie)):
        if kategorie[i].text == u"Aktywność":
            akty.append([th[i].a.text, termin[i].text, scores[i].text.strip()])
        elif kategorie[i].text  == "Egzamin":
            egz.append([th[i].a.text, termin[i].text, scores[i].text.strip()])
        elif kategorie[i].text == "Laboratoria":
            lab.append([th[i].a.text, termin[i].text, scores[i].text.strip()])
        else:
            pd.append([th[i].a.text, termin[i].text, scores[i].text.strip()])

    # Sortowanie

    #current_task.update_state(state='PROGRESS', meta={'current': 75, 'total': 100})
    #time.sleep(0.5)

    akty.sort(key=lambda x: x[1])
    pd.sort(key=lambda x: x[1])
    egz.sort(key=lambda x: x[1])
    lab.sort(key=lambda x: x[1])



    return {'aktywnosc': akty, 'prace_domowe': pd, 'egzamin': egz, 'laboratoria': lab}


class InMemoryZip(object):
    def __init__(self):
        self.in_memory_zip = StringIO.StringIO()

    def append(self, filename_in_zip, file_contents):
        zf = zipfile.ZipFile(self.in_memory_zip, "a", zipfile.ZIP_DEFLATED, False)
        zf.writestr(filename_in_zip, file_contents)
        for zfile in zf.filelist:
            zfile.create_system = 0

        return self

    def read(self):
        self.in_memory_zip.seek(0)
        return self.in_memory_zip.read()
@shared_task()
def create_zip(aktywnosc, pd, egzamin, lab):

    tekstA = ""
    tekstP = ""
    tekstE = ""
    tekstL = ""

    for nazwa, czas, punkty in aktywnosc:
        tekstA += nazwa + " " + czas + " " + punkty + "\r\n"

    for nazwa, czas, punkty in pd:
        tekstP += nazwa + " " + czas + " " + punkty + "\r\n"

    for nazwa, czas, punkty in egzamin:
            tekstE += nazwa + " " + czas + " " + punkty + "\r\n"

    for nazwa, czas, punkty in lab:
        tekstL += nazwa + " " + czas + " " + punkty + "\r\n"


    #current_task.update_state(state='PROGRESS', meta={'current': 35, 'total': 100})
    #time.sleep(0.5)

    imz = InMemoryZip()
    imz.append("ocenyAktywnosc.txt", tekstA.encode('utf-8'))
    imz.append("ocenyPD.txt", tekstP.encode('utf-8'))
    imz.append("ocenyEgzamin.txt", tekstE.encode('utf-8'))
    imz.append("ocenyLab.txt", tekstL.encode('utf-8'))

    #current_task.update_state(state='PROGRESS', meta={'current': 75, 'total': 100})
    #time.sleep(0.5)

    return imz.read()

def show_time_left(zalogowany):

    czas = zalogowany.poczatek
    teraz = timezone.now()

    time_left = teraz-czas

    return time_left